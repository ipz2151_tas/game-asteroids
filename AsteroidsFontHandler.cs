﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASTEROIDS
{
    class AsteroidsFontHandler
    {

        private frmAsteroids canvas;

        private int textWidth;
        public int TextWidth { get { return textWidth; } }
        private int textHeight;
        public int TextHeight { get { return textHeight; } }
        private int textLeft;
        public int TextLeft { get { return textLeft; } }
        private int textTop;
        public int TextTop { get { return textTop; } }

        private int kerning;
        public int Kerning { 
            get { return kerning; } 
            set { kerning = value; } 
        }

        public Point TextPosition
        {
            set 
            { 
                textLeft = value.X; 
                textTop = value.Y; 
            }
        }
        private string text;

        private int charSize;
        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                textWidth = (int)((charSize + kerning) * text.Length);
            }
        }

        private float fontSize;
        public float FontSize
        {
            get { return fontSize; }
            set
            {
                fontSize = value;
                charSize = (int)(0.7f * fontSize);
            }

        }

        public AsteroidsFontHandler(frmAsteroids frm)
        {
            canvas = frm;
        }

        private List<Point> secondaryPoints = new List<Point>(); 
        public void Draw()
        {
            Pen p = new Pen(Color.White);
            var ca = text.ToCharArray();

            for (int x = 0; x < ca.Length; x++)
            {
                char s = ca[x];
                if (s != '.' && s != ' ')
                {
                    canvas.g.DrawLines(p, GetPoints(s, x));
                    if (secondaryPoints.Count > 0)
                    {
                        canvas.g.DrawLines(p, secondaryPoints.ToArray());
                        secondaryPoints.Clear();
                    }

                }
                else if (s == '.')
                {
                    calcOffsetsAndStoreInSecondaryPoints(x);
                    canvas.g.FillRectangle(p.Brush, secondaryPoints[0].X, secondaryPoints[0].Y, 1, 1);
                    secondaryPoints.Clear();
                }
            }
            return;
        }

        private void calcOffsetsAndStoreInSecondaryPoints(int ox)
        {
            int lx = ox * (charSize + kerning);
            int fs = (int)FontSize;

            secondaryPoints.Add(new Point(textLeft + lx, textTop + fs));
        }

        private Point[] GetPoints(char current, int ox)
        {
            
            List<Point> points = new List<Point>();
             //int lx = ox * (charSize + (int) ( (float)charSize / (float)2.5f ) );
            int lx = ox * (charSize + kerning);
            //(charSize + (int)((float)charSize / (float)2.5f));

            int fs = (int)FontSize;
            switch (current)
            {
                case 'A':
                case 'a':
                case 'А':
                case 'а':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx, textTop + (fs/3)),
                                        new Point(textLeft + lx + (charSize/2), textTop),
                                        new Point(textLeft + lx + charSize, textTop + (fs/3)),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + ((2*fs)/3)),
                                        new Point(textLeft + lx, textTop + ((2*fs)/3))
                                    });
                    break;

                case 'B':
                case 'b':
                case 'В':
                case 'в':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx + (3*charSize/4), textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize, textTop + ((2*fs)/3)),
                                        new Point(textLeft + lx + charSize, textTop + ((5*fs)/6)),
                                        new Point(textLeft + lx + (3*charSize/4), textTop + fs),
                                        new Point(textLeft + lx + (3*charSize/4), textTop + fs),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + (3*charSize/4), textTop),
                                        new Point(textLeft + lx + charSize, textTop + (fs/6)),
                                        new Point(textLeft + lx + charSize, textTop + (fs/3)),
                                        new Point(textLeft + lx + (3*charSize/4), textTop + (fs/2))

                                    });
                    break;

                case 'C':
                case 'c':
                case 'С':
                case 'с':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + fs)
                                    });
                    break;

                case 'D':
                case 'd':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + (charSize/2), textTop),
                                        new Point(textLeft + lx + charSize, textTop +(fs/3)),
                                        new Point(textLeft + lx + charSize, textTop +(2*fs/3)),
                                        new Point(textLeft + lx + (charSize/2), textTop + fs),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx, textTop)
                                    });
                    break;

                case 'E':
                case 'e':
                case 'Е':
                case 'е':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx + (3*charSize/4) , textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + fs)
                                    });

                    break;
                case 'F':
                case 'f':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx + (3 * charSize/4) , textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + fs)
                                    });
                    break;

                case 'G':
                case 'g':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx + charSize, textTop + (fs/3)),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + (2*fs/3)),
                                        new Point(textLeft + lx + (charSize/2), textTop + (2*fs/3))
                                    });
                    break;

                case 'H':
                case 'h':
                case 'Н':
                case 'н':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx, textTop + fs/2),
                                        new Point(textLeft + lx + charSize , textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize , textTop),
                                        new Point(textLeft + lx + charSize , textTop + fs)
                                    });
                    break;

                case 'I':
                case 'i':
                case 'І':
                case 'і':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + (charSize/2), textTop),
                                        new Point(textLeft + lx + (charSize/2), textTop + fs),
                                        new Point(textLeft + lx, textTop +fs),
                                        new Point(textLeft + lx + charSize, textTop +fs)
                                    });
                    break;


                case 'J':
                case 'j':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx + (charSize/2), textTop + fs),
                                        new Point(textLeft + lx, textTop + (2*fs/3))
                                    });
                    break;

                case 'K':
                case 'k':
                case 'К':
                case 'к':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize, textTop + fs)
                                    });
                    break;

                case 'L':
                case 'l':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + charSize , textTop + fs)
                                    });
                    break;

                case 'M':
                case 'm':
                case 'М':
                case 'м':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + (charSize/2), textTop + (fs/3)),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + fs)
                                    });
                    break;


                case 'N':
                case 'n':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + fs)
                                    });
                    break;

                case 'P':
                case 'p':
                case 'Р':
                case 'р':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + (fs/2))
                                    });
                    break;
                case 'Q':
                case 'q':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx + (charSize/2), textTop + (2*fs/3)),
                                        new Point(textLeft + lx + (3*charSize/4), textTop + (5*fs/6)),
                                        new Point(textLeft + lx + (charSize/2), textTop + fs),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + (2*fs/3)),
                                        new Point(textLeft + lx + (3*charSize/4), textTop + (5*fs/6))
                                    });
                    break;
                case 'R':
                case 'r':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize, textTop + fs)
                                    });
                    break;

                case 'S':
                case 's':
                case '5':
                    points.AddRange(new[]
                                        {
                                                new Point(textLeft + lx, textTop + fs),
                                                new Point(textLeft + lx + charSize, textTop + fs),
                                                new Point(textLeft + lx + charSize, textTop + (fs/2)),
                                                new Point(textLeft + lx, textTop + fs/2),
                                                new Point(textLeft + lx, textTop),
                                                new Point(textLeft + lx + charSize, textTop)

                                    });
                    break;
                case 'T':
                case 't':
                case 'Т':
                case 'т':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + (charSize/2), textTop),
                                        new Point(textLeft + lx + (charSize/2), textTop + fs)
                                    });
                    break;

                case 'U':
                case 'u':

                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop)
                                    });
                    break;

                case 'V':
                case 'v':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + (charSize/2), textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop)
                                    });
                    break;

                case 'W':
                case 'w':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + (charSize/2), textTop + (2*fs/3)),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop)
                                    });
                    break;

                case 'X':
                case 'x':
                case 'Х':
                case 'х':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx + (charSize/2), textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx, textTop + fs)
                                    });
                    break;

                case 'Y':
                case 'y':
                
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + (charSize/2), textTop + (fs/3)),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + (charSize/2), textTop + (fs/3)),
                                        new Point(textLeft + lx + (charSize/2), textTop + fs)
                                    });
                    break;

                case 'Z':
                case 'z':
                    points.AddRange(new[]
                                        {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + fs)
                                    });
                    break;

                
                case '0':
                case 'O':
                case 'o':
                case 'О':
                case 'о':
                    points.AddRange(new[]
                                    {   new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx, textTop)
                                    });
                    break;

                case '1':
                    points.AddRange(new[]
                                    {
                                        new Point(textLeft + lx + (charSize/2), textTop),
                                        new Point(textLeft + lx + (charSize/2), textTop + fs),
                                    });
                    break;

                case '2':
                    points.AddRange(new[]
                                    {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                    });
                    break;

                case '3':
                case 'з':
                case 'З':
                    points.AddRange(new[]
                                    {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize, textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx, textTop + fs),
                                    });
                    break;

                case '4':
                    points.AddRange(new[]
                                    {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize, textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + fs)
                                    });
                    break;

                case '6':
                    points.AddRange(new[]
                                    {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + (fs/2))
                                    });
                    break;

                case '7':
                    points.AddRange(new[]
                                    {
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + fs)
                                    });
                    break;

                case '8':
                    points.AddRange(new[]
                                    {
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize, textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                    });
                    break;

                case '9':
                    points.AddRange(new[]
                                    {
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx, textTop + (fs/2)),
                                        new Point(textLeft + lx + charSize, textTop + (fs/2))
                                    });
                    break;

                case '©':
                    points.AddRange(new[]
                                    {
                                        new Point(textLeft + lx + (charSize/4), textTop),
                                        new Point(textLeft + lx, textTop + (fs/6)),
                                        new Point(textLeft + lx, textTop + (5*fs/6)),
                                        new Point(textLeft + lx + (charSize/4), textTop + fs),
                                        new Point(textLeft + lx + (3*charSize/4), textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + (5*fs/6)),
                                        new Point(textLeft + lx + charSize, textTop + (fs/6)),
                                        new Point(textLeft + lx + (3*charSize/4), textTop),
                                        new Point(textLeft + lx + (charSize/4), textTop)
                                    });

                    secondaryPoints.AddRange(new[]
                                    {
                                        new Point(textLeft + lx + (3*charSize/4), textTop + (fs/6)),
                                        new Point(textLeft + lx + (charSize/4), textTop + (fs/6)),
                                        new Point(textLeft + lx + (charSize/4), textTop + (5*fs/6)),
                                        new Point(textLeft + lx + (3*charSize/4), textTop + (5*fs/6))
                                    });
                    break;

                case '_':
                    points.AddRange(new[]
                                    {
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop + fs)
                                    });
                    break;
               
                
                case 'Б':
                case 'б':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx + charSize, textTop),
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx, textTop + fs),
                        new Point(textLeft + lx + charSize, textTop + fs),
                        new Point(textLeft + lx + charSize, textTop + (fs / 2)),
                        new Point(textLeft + lx, textTop + (fs / 2))
                    });
                    break;

                
                case 'Г':
                case 'г':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx, textTop + fs),
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx + charSize, textTop)
                    });
                    break;


                
                case 'Д':
                case 'д':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx, textTop + fs),
                        new Point(textLeft + lx, textTop + 3 * fs / 4),
                        new Point(textLeft + lx + charSize, textTop + 3 * fs / 4),
                        new Point(textLeft + lx + charSize, textTop + fs),
                        new Point(textLeft + lx + charSize, textTop + 3 * fs / 4),
                        new Point(textLeft + lx + 3 * charSize / 4, textTop + 3 * fs / 4),
                        new Point(textLeft + lx + charSize / 2, textTop),
                        new Point(textLeft + lx + charSize / 4, textTop + 3 * fs / 4)
                    });
                    break;

                
                case 'Є':
                case 'є':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx + charSize, textTop),
                        new Point(textLeft + lx + charSize / 4, textTop),
                        new Point(textLeft + lx, textTop + fs / 2),
                        new Point(textLeft + lx + 3 * charSize / 4, textTop + fs / 2),
                        new Point(textLeft + lx, textTop + fs / 2),
                        new Point(textLeft + lx + charSize / 4, textTop + fs),
                        new Point(textLeft + lx + charSize, textTop + fs),
                    });
                    break;
               
                case 'Ж':
                case 'ж':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx + charSize, textTop + fs),
                        new Point(textLeft + lx + charSize / 2, textTop + fs / 2),
                        new Point(textLeft + lx, textTop + fs),
                        new Point(textLeft + lx + charSize, textTop),
                        new Point(textLeft + lx + charSize / 2, textTop + fs / 2),
                        new Point(textLeft + lx + charSize / 2, textTop),
                        new Point(textLeft + lx + charSize / 2, textTop+fs)
                    });
                    break;

                
                case 'И':
                case 'и':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx, textTop + fs),
                        new Point(textLeft + lx + charSize, textTop),
                        new Point(textLeft + lx + charSize, textTop + fs)
                    });
                    break;

                
                case 'Л':
                case 'л':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx, textTop + fs),
                        new Point(textLeft + lx, textTop + (1 * fs / 3)),
                        new Point(textLeft + lx + (1 * charSize / 3), textTop),
                        new Point(textLeft + lx + charSize, textTop),
                        new Point(textLeft + lx + charSize, textTop + fs)
                    });
                    break;

               
                case 'П':
                case 'п':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx, textTop + fs),
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx + charSize, textTop),
                        new Point(textLeft + lx + charSize, textTop + fs)
                    });
                    break;

                
                case 'У':
                case 'у':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx, textTop + fs / 3),
                        new Point(textLeft + lx + (1 * charSize / 3), textTop + fs / 2),
                        new Point(textLeft + lx + charSize, textTop + fs / 2),
                        new Point(textLeft + lx + charSize, textTop),
                        new Point(textLeft + lx + charSize, textTop + 2 * fs / 3),
                        new Point(textLeft + lx + 2 * charSize / 3, textTop + fs),
                        new Point(textLeft + lx, textTop + fs)
                    });
                    break;

                
                case 'Ф':
                case 'ф':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx + charSize / 2, textTop + fs),
                        new Point(textLeft + lx + charSize / 2, textTop),
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx, textTop + fs / 2),
                        new Point(textLeft + lx + charSize, textTop + fs / 2),
                        new Point(textLeft + lx + charSize, textTop),
                        new Point(textLeft + lx + charSize / 2, textTop)
                    });
                    break;

               
                case 'Ц':
                case 'ц':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx + charSize, textTop + fs),
                        new Point(textLeft + lx + charSize, textTop + 3 * fs / 4),
                        new Point(textLeft + lx + 3 * charSize / 4, textTop + 3 * fs / 4),
                        new Point(textLeft + lx + 3 * charSize / 4, textTop),
                        new Point(textLeft + lx + 3 * charSize / 4, textTop + 3 * fs / 4),
                        new Point(textLeft + lx, textTop + 3 * fs / 4),
                        new Point(textLeft + lx, textTop)
                    });
                    break;

                
                case 'Ч':
                case 'ч':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx, textTop + (1 * fs / 3)),
                        new Point(textLeft + lx + (1 * charSize / 3), textTop + fs / 2),
                        new Point(textLeft + lx + charSize, textTop + fs / 2),
                        new Point(textLeft + lx + charSize, textTop),
                        new Point(textLeft + lx + charSize, textTop + fs),
                    });
                    break;

                
                case 'Ш':
                case 'ш':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx, textTop + fs),
                        new Point(textLeft + lx + (charSize / 2), textTop + fs),
                        new Point(textLeft + lx + (charSize / 2), textTop),
                        new Point(textLeft + lx + (charSize / 2), textTop + fs),
                        new Point(textLeft + lx + charSize, textTop + fs),
                        new Point(textLeft + lx + charSize, textTop),
                    });
                    break;

                
                case 'Щ':
                case 'щ':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx + charSize, textTop + fs),
                        new Point(textLeft + lx + charSize, textTop + 3 * fs / 4),
                        new Point(textLeft + lx + 3 * charSize / 4, textTop + 3 * fs / 4),
                        new Point(textLeft + lx + 3 * charSize / 4, textTop),
                        new Point(textLeft + lx + 3 * charSize / 4, textTop + 3 * fs / 4),
                        new Point(textLeft + lx + 2 * charSize / 5, textTop + 3 * fs / 4),
                        new Point(textLeft + lx + 2 * charSize / 5, textTop),
                        new Point(textLeft + lx + 2 * charSize / 5, textTop + 3 * fs / 4),
                        new Point(textLeft + lx, textTop + 3 * fs / 4),
                        new Point(textLeft + lx, textTop)
                    });
                    break;

              
                case 'Ю':
                case 'ю':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx, textTop + fs),
                        new Point(textLeft + lx, textTop + fs / 2),
                        new Point(textLeft + lx + 1 * charSize / 4, textTop + fs / 2),
                        new Point(textLeft + lx + 1 * charSize / 4, textTop),
                        new Point(textLeft + lx + charSize, textTop),
                        new Point(textLeft + lx + charSize, textTop + fs),
                        new Point(textLeft + lx + 1 * charSize / 4, textTop + fs),
                        new Point(textLeft + lx + 1 * charSize / 4, textTop + fs / 2),
                    });
                    break;

                
                case 'Я':
                case 'я':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx + charSize, textTop + fs),
                        new Point(textLeft + lx + charSize, textTop),
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx, textTop + fs / 2),
                        new Point(textLeft + lx + charSize, textTop + fs / 2),
                        new Point(textLeft + lx, textTop + fs)
                    });
                    break;
               
                case 'Ь':
                case 'ь':
                    points.AddRange(new[]
                    {
                        new Point(textLeft + lx, textTop),
                        new Point(textLeft + lx, textTop + fs),
                        new Point(textLeft + lx + charSize, textTop+fs),
                        new Point(textLeft + lx + charSize, textTop+fs/2),
                        new Point(textLeft + lx, textTop + fs/2),
                    });
                    break;



                default:
                    points.AddRange(new[]
                                    {   new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx + charSize, textTop),
                                        new Point(textLeft + lx + charSize, textTop + fs),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx, textTop),
                                        new Point(textLeft + lx+charSize, textTop + fs),
                                        new Point(textLeft + lx, textTop + fs),
                                        new Point(textLeft + lx + charSize, textTop)
                                    });
                    break;

            }
            return points.ToArray();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace ASTEROIDS
{
    public class HighScores
    {
        private int MAXHIGHSCORES = 9;
        private List<HighScore> lstHighScores = new List<HighScore>();
        private String HighScoresPath = Directory.GetCurrentDirectory() + "\\HighScores.DAT";

        public HighScores()
        {
            Load();
        }

        public List<HighScore> list
        {
            get { return lstHighScores; }
        }

        public void Save()
        {
            List<string> lines = new List<string>();
            foreach (HighScore hs in lstHighScores)
            {
                lines.Add(String.Format("{0}{1}", hs.Initials, hs.Score));
            }
            File.WriteAllLines(@HighScoresPath, lines.ToArray(), Encoding.UTF8);

            return;
        }

        public bool Load()
        {
            lstHighScores.Clear();
            if (File.Exists(HighScoresPath))
            {
                string[] lines = System.IO.File.ReadAllLines(@HighScoresPath);

                
                for (int nIndex = 0; nIndex < lines.Length; nIndex++)
                {
                     
                    if (nIndex >= MAXHIGHSCORES)
                        break;

                    HighScore hs = new HighScore();
                    hs.Initials = lines[nIndex].Substring(0, 3);
                    hs.Score = System.Convert.ToInt32(lines[nIndex].Substring(3, lines[nIndex].Length - 3));
                    lstHighScores.Add(hs);
                }
            }

            return true;
        }

        public bool isHighScore(int score)
        {
            bool isHighScore = false;
            if (lstHighScores.Count < MAXHIGHSCORES) isHighScore = true;
            else
            {
                for (int nIndex = 0; nIndex < lstHighScores.Count; nIndex++)
                {
                    if (lstHighScores[nIndex].Score < score)
                    {
                        isHighScore = true;
                        break;
                    }
                }
            }


            return isHighScore;
        }

        public void AddScore(HighScore hs)
        {
            bool isHighScore = false;
            int insertAt = 0;
            for (int nIndex = 0; nIndex < lstHighScores.Count; nIndex++)
            {
                if (lstHighScores[nIndex].Score < hs.Score)
                {
                    isHighScore = true;
                    insertAt = nIndex;
                    break;
                }
            }

            if (lstHighScores.Count < MAXHIGHSCORES)
            {
                lstHighScores.Add(hs);
            }
            else if (isHighScore == true) 
            {
                lstHighScores.Insert(insertAt, hs);              
                lstHighScores.RemoveAt(lstHighScores.Count-1);   
            }
            Save(); Load();
        }

        public int GetHighScore()
        {
            int nScore = 0;
            if (lstHighScores.Count > 0)
                nScore = lstHighScores.OrderByDescending(hs => hs.Score).ToList()[0].Score;

            return nScore;
        }

    }

    public class HighScore
    {
        private int curInitialIndex = 0; 

        private string initials = "A__";
        public string Initials
        {
            get { return initials; }
            set { initials = value; }
        }
        private int score;
        public int Score
        {
            get { return score; }
            set { score = value; }
        }

        public HighScore()
        {
        }
    }

    
    public class EnterHighScoreScreen
    {
        public frmAsteroids canvas;
        private int curIndex = 0; 
        public bool display = false;
        public HighScore newHighScore = new HighScore();

        public bool IsDone
        {
            get { return curIndex>2;  }
        }

        public EnterHighScoreScreen(frmAsteroids frm)
        {
            canvas = frm;
            newHighScore.Initials = "A__";
        }

        public void Draw()
        {
            if (display)
            {
                
                AsteroidsFontHandler afh = new AsteroidsFontHandler(canvas);
                System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.White);

                int topPosOfBottomRowOfHelpText = 485;

                
                afh.FontSize = 32.0f; afh.Kerning = 10;
                afh.Text = "НАТИСНІТЬ ЛІТЕРУ ";
                afh.TextPosition = new Point(((canvas.Width - afh.TextWidth) / 2), topPosOfBottomRowOfHelpText);
                afh.Draw();

                int left = afh.TextLeft;
                afh.Text = "НАТИСНІТЬ ОБЕРТАННЯ ДЛЯ ВИБОРУ ЛІТЕРИ";
                afh.TextPosition = new Point(left, topPosOfBottomRowOfHelpText -= 50);
                afh.Draw();

                afh.Text = "ВВЕДІТЬ ВЛАСНІ ІНІЦІАЛИ";
                afh.TextPosition = new Point(left, topPosOfBottomRowOfHelpText -= 50);
                afh.Draw();

                afh.Text = "ВАШІ БАЛИ В ТОП 10";
                afh.TextPosition = new Point(left, topPosOfBottomRowOfHelpText -= 50);
                afh.Draw();

               
                afh.FontSize = 50.0f; afh.Kerning = 20;
                afh.Text = newHighScore.Initials;
                
                afh.TextPosition = new Point(((canvas.Width - afh.TextWidth) / 2) - 28, canvas.Height - 215);
                afh.Draw();
            }
        }

        public bool ProcessKey(Keys key )
        {
            bool bProcessed = false;
            if (!IsDone)
            {
                char cur = newHighScore.Initials[curIndex];
                char[] allInitials = newHighScore.Initials.ToCharArray();
                string allChars = "_ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                int indexofCurChar = allChars.IndexOf(cur);

                switch (key)
                {
                    case Keys.Left:
                        if (indexofCurChar == 0)
                            indexofCurChar = allChars.Length;
                        cur = allChars.ToCharArray()[indexofCurChar - 1];
                        allInitials[curIndex] = cur;
                        newHighScore.Initials = string.Join("", allInitials);
                        bProcessed = true;
                        break;
                    case Keys.Right:
                        if (indexofCurChar == allChars.Length - 1)
                            indexofCurChar = -1;
                        cur = allChars.ToCharArray()[indexofCurChar + 1];
                        allInitials[curIndex] = cur;
                        newHighScore.Initials = string.Join("", allInitials);
                        bProcessed = true;
                        break;
                    case Keys.Down:
                        curIndex += 1;
                        bProcessed = true;
                        break;
                }
            }

            return bProcessed;
        }
    }
}
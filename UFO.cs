﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASTEROIDS
{
    class UFO
    {
        private frmAsteroids canvas;

        private bool leftToRight;
        public float myAngle = 0.0f;
        
        //public float velocity;
        public const float SMALLSPEED = 5.0f;
        public const float LARGESPEED = 4.5f; 
        private const long TICKSPERSECOND = 10000000;
        public const float MAXVELOCITY = 0.8f;
        public const int RADIUS = 27; 
        public const int XMARGIN = 200;  

        public Point collisionLocation;
        public Animations destructionAnimation = new Animations(Animations.ANIMTYPE.GENERAL);


        private const int MAXDIRECTIONALCHANGES = 5;
        private const long MININTERVALFORDIRECTIONALCHANGE = TICKSPERSECOND * 2;
        private int directionalChanges = 0;
        private const float PROBABILITYOFDIRECTIONCHANGESTART = 5.0f;
        private const float PROBABILITYOFDIRECTIONCHANGEINCREASEPERTICK = 0.1f;

        private float probabilityOfDirectionChange = PROBABILITYOFDIRECTIONCHANGESTART;
        private long nextPossibleDirectionalChange;

        private float sa;
        private float ca;
        private float forAngularSpeedAdjustmentHack = 90.0f;

        public enum UFOSIZE
        {
            LARGE = 0,
            SMALL = 1
        }

        public UFOSIZE ufoType = UFOSIZE.SMALL;

        public Point position = new Point();
        private bool isActive = false;
        public bool IsActive
        {
            get { return isActive;  }
            set { isActive = value; }
        }

        public bool isHyperSpace = false;

        private long destructionAnimationTickStart;
        private bool ufoIsDestroyed = false;
        public bool UFOIsDestroyed
        {
            get { return ufoIsDestroyed; }
            set
            {
                if (value)
                    destructionAnimationTickStart = DateTime.Now.Ticks;

                ufoIsDestroyed = value;
            }
        }

        public UFO(frmAsteroids frm)
        {
            canvas = frm;
        }

        public void Draw()
        {
            Pen pen = new Pen(Color.White);

            if (ufoIsDestroyed)
            {
                if (destructionAnimation.radius < RADIUS)
                    destructionAnimation.radius += 2.25f;
                else
                {
                    IsActive = false;
                    ufoIsDestroyed = false;
                }

                Brush currentBrush = (Brush)Brushes.White;
                List<Point> daPoints = new List<Point>();
                foreach (Vector2 vec in destructionAnimation.sequence())
                {
                    float ND = -(float)((float)Math.PI / 2.0f);
                    int x = (int)(Math.Cos((vec.Y / 180) * Math.PI + ND) * vec.X * destructionAnimation.radius);
                    int y = (int)(Math.Sin((vec.Y / 180) * Math.PI + ND) * vec.X * destructionAnimation.radius);

                    canvas.g.FillRectangle(currentBrush, collisionLocation.X + x, collisionLocation.Y + y, 2, 2);
                }
            }
            else if (isActive) 
            {
                bool shouldIChangeDirection = (canvas.randomizer.Next(1,(int)probabilityOfDirectionChange) ==1)?true:false;
                
                if (shouldIChangeDirection )
                    probabilityOfDirectionChange = PROBABILITYOFDIRECTIONCHANGESTART;
                else if(probabilityOfDirectionChange > 2)
                    probabilityOfDirectionChange -= PROBABILITYOFDIRECTIONCHANGEINCREASEPERTICK;

                if ((nextPossibleDirectionalChange < DateTime.Now.Ticks) &&
                     (shouldIChangeDirection) &&
                     (directionalChanges < MAXDIRECTIONALCHANGES))
                {
                    directionalChanges += 1;

                stuffWithRandomNumbers:
                    int direction= canvas.randomizer.Next(1, 4);

                    if (direction< 1 || direction> 3) goto stuffWithRandomNumbers;

                    forAngularSpeedAdjustmentHack = ( 45 * (int)direction);

                    myAngle = ( ( leftToRight )?1: -1 ) * (float) ( (float)forAngularSpeedAdjustmentHack * ( Math.PI / 180.0f) );

                    sa = (float) Math.Sin(myAngle);
                    ca = (float) Math.Cos(myAngle);

                    nextPossibleDirectionalChange = DateTime.Now.Ticks + MININTERVALFORDIRECTIONALCHANGE;
                }
                 
                if (   ( ( ( ( position.X + XMARGIN ) > canvas.Width ) && leftToRight ) 
                    ||   ( ( ( position.X - XMARGIN ) < 0) ) && !leftToRight )  &&
                         ( Math.Abs(myAngle) != 90.0f ) )
                {
                    myAngle = ((leftToRight) ? 1 : -1) * (float)((float)90.0f * (Math.PI / 180.0f));

                    sa = (float)Math.Sin(myAngle);
                    ca = (float)Math.Cos(myAngle);
                }

                position.X += (int) ( (float) ( (leftToRight) ? 1 : -1)  *((ufoType == UFOSIZE.LARGE) ? LARGESPEED : SMALLSPEED ) );
                position.Y += (int)(ca * ((float)((ufoType == UFOSIZE.LARGE) ? LARGESPEED : SMALLSPEED)));

                if (position.Y < 0) 
                    position.Y = canvas.Height + position.Y;

                if (position.Y > canvas.Height) 
                    position.Y = position.Y - canvas.Height;

                if (position.X - RADIUS > canvas.Width || position.X + RADIUS < 0)
                {
                    isActive = false;
                    canvas.onUFOExit();
                }
                else
                {
                    List<Point> points = new List<Point>();

                    int px = position.X;
                    int py = position.Y;

                    if (ufoType == UFOSIZE.LARGE)
                    {
                        points.AddRange(new[]
                                            {
                                        new Point(px + 27, py),
                                        new Point(px + 10, py-9),
                                        new Point(px + 5, py-17),
                                        new Point(px - 5, py-17),
                                        new Point(px - 10, py-9),
                                        new Point(px + 10, py-9),
                                        new Point(px - 10, py-9),
                                        new Point(px - 27, py),
                                        new Point(px + 27, py),
                                        new Point(px + 10, py +9),
                                        new Point(px - 10, py +9),
                                        new Point(px - 27, py),
                                    });
                    }
                    else 
                    {
                        points.AddRange(new[]
                                            {
                                        new Point(px + 27/2, py),
                                        new Point(px + 10/2, py-9/2),
                                        new Point(px + 5/2, py-17/2),
                                        new Point(px - 5/2, py-17/2),
                                        new Point(px - 10/2, py-9/2),
                                        new Point(px + 10/2, py-9/2),
                                        new Point(px - 10/2, py-9/2),
                                        new Point(px - 27/2, py),
                                        new Point(px + 27/2, py),
                                        new Point(px + 10/2, py +9/2),
                                        new Point(px - 10/2, py +9/2),
                                        new Point(px - 27/2, py),
                                    });
                    }

                    canvas.g.DrawLines(pen, points.ToArray());
                }
                
            }
        }

        

         
        public void spawnUFO(int level, long levelTime)
        {
            if ( !isActive ) 
            {
                
                ufoType = (((levelTime / TICKSPERSECOND) * level) < 45) ? UFOSIZE.LARGE : UFOSIZE.SMALL;

                
                leftToRight = ( canvas.randomizer.Next(1,100) < 50 )? true: false;

                
                position = new Point((leftToRight) ? -RADIUS : canvas.Width + RADIUS, RADIUS + canvas.randomizer.Next(canvas.Height - (2*RADIUS)));

                nextPossibleDirectionalChange = DateTime.Now.Ticks + MININTERVALFORDIRECTIONALCHANGE;
                directionalChanges = 0;

                
                myAngle = ((leftToRight) ? 1 : -1) * (float)( Math.PI/2 );

                
                sa = (float)Math.Sin(myAngle);
                ca = (float)Math.Cos(myAngle);

                
                isActive = true;
            }

        }

        public bool doesObjectCollide(Point p, float objectradius)
        {
            float checkVal = (float)Math.Sqrt(Math.Pow(position.X - p.X, 2) + Math.Pow(position.Y - p.Y, 2));
            if (checkVal < objectradius)
            {
                collisionLocation = position;
                destructionAnimation.radius = 0.0f;
                return true;
            }

            return false;
        }

        public void triggerCollisionSequence()
        {
            collisionLocation = position;
            destructionAnimation.radius = 0.0f;
            isActive = false;
            ufoIsDestroyed = true;
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace ASTEROIDS
{
    public partial class frmAsteroids : Form
    {   
        private int X = 0;
        private int Y = 0;

        private const int DLEN = 2;
        private const long DELAYBETWEElEVELS = 25000000;
        private const float PLAYERRESURRECTIONDELAY = 3.0f;
        public const long TICKSPERSECOND = 10000000;
        public Random randomizer = new Random(DateTime.Now.TimeOfDay.Milliseconds);


        private bool currentLevelTransition = false;

        private List<Asteroid> asteroids = new List<Asteroid>();
        private Ship player;
        private Projectiles projectiles; private bool isSpacePressed = false;
        private Projectiles ufoprojectiles; 
        private ScoreBoard mainScoreBoard;
        public EnterHighScoreScreen EnterHighScoreScreen;
        public bool? displayEnterHighScoreScreen = null;
        private const double TIMEOUTBEFOREDISPLAYINGHIGHSCORESCREEN = 3.0f;
        private const double TIMEOUTFORHIGHSCOREDISPLAYSCREEN = 30.0f;
        private long ticksHighScoreDisplayScreenTimesOut;

        private UFO ufo;

        public Graphics g;
        public float ASPECTRATIO;
        public string DEBUGOUTPUT = "";
        private long ticksSinceStart;

        private long[] playerDestroyedSFTicks = new long[2];
        private long UFOFiringSFTicks;
        private long[] playerInHyperSpaceSFTicks = new long[2];

        private int numDemoScreensteroids = 10;

        public bool gameOver = false;
        public long gameOverResetAtTicks = 0;

        public long NextRandomUFOEncounter;

        public Sounds mySounds;

        
        private long tckNextBGSound;
        private bool BGSoundLow = false;
        private bool PlayerIsThrusting = false;
        private double AmbientAccelerator = 1.0; 

        public frmAsteroids()
        {
            InitializeComponent();

            
            System.Windows.Forms.Cursor.Hide();
            this.Width = Screen.PrimaryScreen.Bounds.Width;
            this.Height = Screen.PrimaryScreen.Bounds.Height;
            this.Top = 0;
            this.Left = 0;

            ASPECTRATIO = (float)Screen.PrimaryScreen.Bounds.Height / (float)Screen.PrimaryScreen.Bounds.Width;

            timer1.Interval = (int)(1000.0f / 60.0f);

            //g = this.CreateGraphics();
            player = new Ship(this);
            mainScoreBoard = new ScoreBoard(this);
            projectiles = new Projectiles(this);
            ufoprojectiles = new Projectiles(this);
            ufo = new UFO(this);
            EnterHighScoreScreen = new EnterHighScoreScreen(this);
            mySounds = new Sounds(this);
            ufo.position = new Point(250, 250); 

            //this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            for (int x = 0; x < numDemoScreensteroids; x++)
            {
                asteroids.Add(new Asteroid(this, (Asteroid.SIZEOFASTEROID)randomizer.Next(0,3) ));
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            long currentTicks = System.DateTime.Now.Ticks;
            if (mainScoreBoard.isPlaying && !gameOver)
            {
               
                if (asteroids.Count > 0 && !player.isActive)
                {
                    asteroids.Clear();
                    mainScoreBoard.SCORE = 0; 
                }
                
                else if ((currentTicks - ticksSinceStart) > 20000000 && !player.isActive)
                {
                    player.isActive = true;
                    mainScoreBoard.PLAYERMESSAGE = "";

                    mainScoreBoard.CURRENTLEVEL = 1;
                    AmbientAccelerator = 1.0;
                    NextRandomUFOEncounter = System.DateTime.Now.AddTicks(TICKSPERSECOND * randomizer.Next(5, 15)).Ticks;
                    loadAsteroidsForThisLevel();
                }
                
                else if (asteroids.Count == 0 && player.isActive && !currentLevelTransition)
                {
                    ticksSinceStart = System.DateTime.Now.Ticks;

                    mainScoreBoard.CURRENTLEVEL += 1;
                    AmbientAccelerator = 1.0;

                    switch (mainScoreBoard.CURRENTLEVEL)
                    {
                        case 2:
                            NextRandomUFOEncounter = System.DateTime.Now.AddTicks( TICKSPERSECOND * randomizer.Next(10, 30) ).Ticks;
                            break;
                        default:
                            NextRandomUFOEncounter = System.DateTime.Now.AddTicks(TICKSPERSECOND * randomizer.Next(10, 30)).Ticks;
                            break;
                    }
                    currentLevelTransition = true;
                }
                
                else if (currentLevelTransition && ((currentTicks - ticksSinceStart) > DELAYBETWEElEVELS))
                {
                    currentLevelTransition = false;
                    loadAsteroidsForThisLevel();
                }

                if (player.isHyperSpace)
                {
                    playerInHyperSpaceSFTicks[1] = DateTime.Now.Ticks;
                    if (playerInHyperSpaceSFTicks[0] < playerInHyperSpaceSFTicks[1])
                    {
                        player.velocity = 0.0f;
                        player.position.X = randomizer.Next(200, Screen.PrimaryScreen.Bounds.Width - 200);
                        player.position.Y = randomizer.Next(250, Screen.PrimaryScreen.Bounds.Height - 250);
                        player.isHyperSpace = false;
                    }
                }

                if (!player.bPlayerIsDestroyed)
                {
                    foreach (Asteroid a in asteroids)
                    {
                        if (player.doesObjectCollide(a.Position, a.myRadius))
                        {
                            if (!player.bPlayerIsDestroyed)
                            {
                                a.playerCollided = true;
                                mySounds.library[Sounds.SOUNDS.DESTROYEDASTEROIDLARGE].Play();
                                initiatePlayerDestroyedSequence();
                            }
                            break;
                        }
                    }
                }
                if (player.bPlayerIsDestroyed)
                {
                    playerDestroyedSFTicks[1] = DateTime.Now.Ticks;
                    if ((playerDestroyedSFTicks[1] - playerDestroyedSFTicks[0]) > (PLAYERRESURRECTIONDELAY * TICKSPERSECOND))
                    {
                        player.resetToCenter();
                        
                        foreach (Asteroid a in asteroids)
                        {
                            if (a.doesPointCollide(player.position))
                                goto somePart;
                        }

                        player.bPlayerIsDestroyed = false;
                        player.velocity = 0.0f;
                    }
                }

            somePart:
                if(ufo.IsActive && !ufo.UFOIsDestroyed)
                {
                    mySounds.library[(ufo.ufoType == UFO.UFOSIZE.LARGE)?Sounds.SOUNDS.SPACESHIPLARGE: Sounds.SOUNDS.SPACESHIPSMALL].Play();

                    
                    double firingRate = (ufo.ufoType == UFO.UFOSIZE.LARGE) ? 0.9 : 0.7;

                    if ((((double) System.DateTime.Now.Ticks - (double) UFOFiringSFTicks) / (double) TICKSPERSECOND) >= firingRate)
                    {
                        UFOFiringSFTicks = System.DateTime.Now.Ticks;

                        
                        int rVal = (ufo.ufoType == UFO.UFOSIZE.LARGE)? randomizer.Next(1, 5) : randomizer.Next(1, 4);
                        bool isRandom = (rVal != 1);

                        if (isRandom)
                            ufoprojectiles.RandomFire(ufo.position);
                        else
                        {
                            float angle = (float) Math.Atan2(player.position.Y - ufo.position.Y,
                                                             player.position.X - ufo.position.X );

                            
                            angle = angle + ( (float) Math.PI / 2.0f ); 
                            

                           ufoprojectiles.NotSoRandomFire(ufo.position, angle);
                        }
                        mySounds.library[Sounds.SOUNDS.PROJECTILE].Play();
                    }

                    if (player.doesObjectCollide(ufo.position, UFO.RADIUS))
                    {
                        initiatePlayerDestroyedSequence();
                        ufo.triggerCollisionSequence();
                        Sounds.SOUNDS currentSnd = (ufo.ufoType == UFO.UFOSIZE.LARGE) ? Sounds.SOUNDS.SPACESHIPLARGE : Sounds.SOUNDS.SPACESHIPSMALL;
                        mySounds.library[Sounds.SOUNDS.DESTROYEDASTEROIDLARGE].Play();
                        mySounds.library[currentSnd].Stop();

                        if (ufo.ufoType == UFO.UFOSIZE.SMALL)
                            mainScoreBoard.SCORE += 1000;
                        else
                            mainScoreBoard.SCORE += 200;
                    }
                    else
                    {
                        foreach (Asteroid a in asteroids)
                        {
                            if (ufo.doesObjectCollide(a.Position, a.myRadius))
                            {
                                ufo.UFOIsDestroyed = true;
                                mySounds.library[(ufo.ufoType == UFO.UFOSIZE.LARGE) ? Sounds.SOUNDS.SPACESHIPLARGE : Sounds.SOUNDS.SPACESHIPSMALL].Stop();
                                a.destroyed = true;
                                if (a.mySize != Asteroid.SIZEOFASTEROID.SMALL)
                                {
                                    for (int x = 0; x < 2; x++)
                                    {
                                        Asteroid newOne = new Asteroid(this, a.mySize - 1, a.Position);
                                        newOne.moveAngle = a.moveAngle + randomizer.Next(-25, 25);
                                        newOne.newPseudoRandomVelocity(a);
                                        asteroids.Add(newOne);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                if(NextRandomUFOEncounter < System.DateTime.Now.Ticks && !ufo.IsActive && player.isActive)
                {
                    UFOFiringSFTicks = System.DateTime.Now.Ticks;
                    if ((System.DateTime.Now.Ticks - ticksSinceStart) < (10 * TICKSPERSECOND))
                        AmbientAccelerator = 1.0;
                    else if ((System.DateTime.Now.Ticks - ticksSinceStart) < (20 * TICKSPERSECOND))
                        AmbientAccelerator = 0.8;
                    else if ((System.DateTime.Now.Ticks - ticksSinceStart) < (30 * TICKSPERSECOND))
                        AmbientAccelerator = 0.6;
                    else
                        AmbientAccelerator = 0.4;

                    ufo.spawnUFO(mainScoreBoard.CURRENTLEVEL, System.DateTime.Now.Ticks - ticksSinceStart);
                    NextRandomUFOEncounter = System.DateTime.Now.AddTicks(TICKSPERSECOND * randomizer.Next(5, 20)).Ticks;
                }
                
                else if ( ufo.IsActive && player.isActive) 
                {
                    NextRandomUFOEncounter = System.DateTime.Now.AddTicks(TICKSPERSECOND * randomizer.Next(5, 15)).Ticks;
                }
            }
            else if (  gameOver ) 
            {
                if(displayEnterHighScoreScreen == null )
                {
                     
                    if (mainScoreBoard.HighScores.isHighScore(mainScoreBoard.SCORE))
                    {
                        displayEnterHighScoreScreen = true;
                        EnterHighScoreScreen = new EnterHighScoreScreen(this);
                        EnterHighScoreScreen.newHighScore.Score = mainScoreBoard.SCORE;
                        ticksHighScoreDisplayScreenTimesOut = System.DateTime.Now.AddTicks(
                            (long)(( TIMEOUTBEFOREDISPLAYINGHIGHSCORESCREEN + TIMEOUTFORHIGHSCOREDISPLAYSCREEN ) * (double)TICKSPERSECOND)).Ticks;
                    }
                    
                    else if ((displayEnterHighScoreScreen == true) && (ticksHighScoreDisplayScreenTimesOut > System.DateTime.Now.Ticks))
                    {
                        
                    }
                    else
                    {
                        displayEnterHighScoreScreen = false;
                    }
                }
                if (displayEnterHighScoreScreen == true)
                {
                    long curTicks = ( ticksHighScoreDisplayScreenTimesOut - DateTime.Now.Ticks ) / TICKSPERSECOND;
                    if (curTicks > TIMEOUTBEFOREDISPLAYINGHIGHSCORESCREEN &&
                       curTicks < TIMEOUTFORHIGHSCOREDISPLAYSCREEN)
                    {
                        gameOver = true;
                        // mainScoreBoard.bIsPlaying = false;
                        player.isActive = false;
                        EnterHighScoreScreen.display = true;
                        if (EnterHighScoreScreen.IsDone)
                        {
                            EnterHighScoreScreen.display = false;
                            mainScoreBoard.HighScores.AddScore(EnterHighScoreScreen.newHighScore);
                            mainScoreBoard.displayHighScoreScreen();
                            displayEnterHighScoreScreen = null;
                            mainScoreBoard.isPlaying = false;
                            player.isActive = false;
                            gameOver = false;
                        }
                    }
                    else if (curTicks <= 0)
                    {
                        displayEnterHighScoreScreen = null;
                        mainScoreBoard.isPlaying = false;
                        player.isActive = false;
                        gameOver = false;
                        EnterHighScoreScreen.display = false;
                    }
                }
                else if (displayEnterHighScoreScreen == false || displayEnterHighScoreScreen == null)
                {
                    if (gameOverResetAtTicks < DateTime.Now.Ticks)
                    {
                        displayEnterHighScoreScreen = null;
                        mainScoreBoard.isPlaying = false;
                        player.isActive = false;
                        gameOver = false;
                    }
                }
            }

           
            if (mainScoreBoard.isPlaying && !gameOver )
            {
                if (tckNextBGSound < System.DateTime.Now.Ticks)
                {
                    tckNextBGSound = System.DateTime.Now.AddTicks(  (long) ( ( double) TICKSPERSECOND * AmbientAccelerator ) ).Ticks;
                    BGSoundLow = !BGSoundLow;
                    mySounds.library[(BGSoundLow) ? Sounds.SOUNDS.BGMUSICLOW : Sounds.SOUNDS.BGMUSICHIGH].Play(); 
                }
                if (!mySounds.library[Sounds.SOUNDS.THRUST].IsPlaying()  && PlayerIsThrusting )
                {
                    mySounds.library[Sounds.SOUNDS.THRUST].Play(); 
                }
            }
            else
                PlayerIsThrusting = false;

        Invalidate();
        }

        private void loadAsteroidsForThisLevel()
        {
            int l=0, m=0, s = 0;

            switch(mainScoreBoard.CURRENTLEVEL)
            {
                case 1:
                    l = 4;
                    break;
                case 2:
                    l = 6;
                    break;
                case 3:
                    l = 8;
                    break;
                case 4:
                    l = 9;
                    break;
                case 5:
                    l = 10;
                    break;

                default:
                    l = 11;  
                             
                    break;
            }
            int cur;
            for (cur = 0; cur < l; cur++)
            {
            tryAgain:
                Asteroid newOne = new Asteroid(this, Asteroid.SIZEOFASTEROID.LARGE);
                if (player.doesObjectCollide(newOne.Position, (int) newOne.myRadius) )
                    goto tryAgain;

                asteroids.Add( newOne );
            }

        }

        private void frmAsteroids_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.D1:
                    if (!mainScoreBoard.isPlaying)
                    {
                        mainScoreBoard.isPlaying = true;
                        mainScoreBoard.SHIPSLEFT = 3;

                        ticksSinceStart = System.DateTime.Now.Ticks;
                    }
                    break;
                case Keys.Up:
                    if (player.isActive)
                    {
                        player.Accelerate(e.KeyCode, true);
                        PlayerIsThrusting = true;
                    }
                    break;
                case Keys.Left:
                case Keys.Right:
                    if (player.isActive)
                        player.Rotate(e.KeyCode, true);
                    else if (displayEnterHighScoreScreen == true)
                        EnterHighScoreScreen.ProcessKey(e.KeyCode);
                    break;
                case Keys.Down:
                    if (player.isActive)
                    {
                        playerInHyperSpaceSFTicks[0] = System.DateTime.Now.Ticks + (randomizer.Next(5, 25) * 1000000);
                        player.isHyperSpace = true;
                    }
                    else if (displayEnterHighScoreScreen == true)
                        EnterHighScoreScreen.ProcessKey(e.KeyCode);

                    break;
                case Keys.Space:
                    if (player.isActive)
                    {
                        if (!isSpacePressed)
                        {
                            projectiles.Fire(player, player.position, player.rotationAngle, player.velocity);
                            mySounds.library[Sounds.SOUNDS.PROJECTILE].Play();
                            isSpacePressed = true;
                        }
                    }
                    break;
            }
        }

        private void frmAsteroids_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    if (player.isActive)
                    {
                        player.Accelerate(e.KeyCode, false);
                        mySounds.library[Sounds.SOUNDS.THRUST].Stop();
                        PlayerIsThrusting = false;
                    }

                    break;
                case Keys.Left:
                case Keys.Right:
                    if (player.isActive)
                        player.Rotate(e.KeyCode, false);
                    break;
                case Keys.Escape:
                    Application.Exit();
                    break;
                case Keys.Space:
                    if (isSpacePressed)
                        isSpacePressed = false;
                    break;
                default:
                    break;
            }
        }

        private void frmAsteroids_Paint(object sender, PaintEventArgs e)
        {
            
            g = e.Graphics;

            //g.Clear(this.BackColor);
            List<Projectile> list = projectiles.list;
            List<Projectile> listUFOProjectiles = ufoprojectiles.list;

            for (int aCur = asteroids.Count-1; aCur >=0  ; aCur--)
            {
                Asteroid a = asteroids[aCur];
                if (!a.destroyed)
                {
                    if (list.Count > 0)
                    {
                        for (int cur = list.Count - 1; cur >= 0; cur--)
                        {
                            if (ufo.IsActive && !ufo.UFOIsDestroyed)
                            {
                                if (ufo.doesObjectCollide(list[cur].position, UFO.RADIUS ))
                                {
                                    mySounds.library[(ufo.ufoType == UFO.UFOSIZE.LARGE)?Sounds.SOUNDS.SPACESHIPLARGE: Sounds.SOUNDS.SPACESHIPSMALL].Stop();
                                    mySounds.library[Sounds.SOUNDS.DESTROYEDASTEROIDLARGE].Play();
                                    if (ufo.ufoType == UFO.UFOSIZE.SMALL)
                                        mainScoreBoard.SCORE += 1000;
                                    else
                                        mainScoreBoard.SCORE += 200;

                                    ufo.UFOIsDestroyed = true;
                                }
                            }

                            if (a.doesPointCollide(list[cur].position) && ! player.bPlayerIsDestroyed )
                            {
                                a.destroyed = true;
                                mainScoreBoard.SCORE += a.ASTEROIDPOINTVALUES[(int)a.mySize];
                                if (a.mySize != Asteroid.SIZEOFASTEROID.SMALL)
                                {
                                    Sounds.SOUNDS nCur = (a.mySize == Asteroid.SIZEOFASTEROID.LARGE) ? Sounds.SOUNDS.DESTROYEDASTEROIDLARGE : Sounds.SOUNDS.DESTROYEDASTEROIDMEDIUM;
                                    mySounds.library[nCur].Play();
                                    for (int x = 0; x < 2; x++)
                                    {
                                        Asteroid newOne = new Asteroid(this, a.mySize - 1, list[cur].position);
                                        newOne.moveAngle = a.moveAngle + randomizer.Next(-25, 25);
                                        newOne.newPseudoRandomVelocity(a);
                                        asteroids.Add(newOne);
                                    }
                                }
                                else
                                    mySounds.library[Sounds.SOUNDS.DESTROYEDASTEROIDSMALL].Play();

                                list.RemoveAt(cur);
                            }
                        }
                    }
                    if(listUFOProjectiles.Count > 0)
                    {
                        for (int cur = listUFOProjectiles.Count - 1; cur >= 0; cur--)
                        {
                            if (player.isActive && !player.bPlayerIsDestroyed)
                            {
                                
                                if (player.doesObjectCollide(listUFOProjectiles[cur].position, 10 ))
                                {
                                    if (!player.bPlayerIsDestroyed)
                                    {
                                        initiatePlayerDestroyedSequence();
                                    }
                                    listUFOProjectiles.RemoveAt(cur);
                                    break;
                                }
                            }

                            if (a.doesPointCollide(listUFOProjectiles[cur].position) )
                            {
                                a.destroyed = true;
                                if (a.mySize != Asteroid.SIZEOFASTEROID.SMALL)
                                {
                                    for (int x = 0; x < 2; x++)
                                    {
                                        Asteroid newOne = new Asteroid(this, a.mySize - 1, listUFOProjectiles[cur].position);
                                        newOne.moveAngle = a.moveAngle + randomizer.Next(-25, 25);
                                        newOne.newPseudoRandomVelocity(a);
                                        asteroids.Add(newOne);
                                    }
                                }
                                listUFOProjectiles.RemoveAt(cur);
                            }
                        }
                    }

                    if (a.playerCollided )
                    {
                        a.destroyed = true;
                        a.collisionLocation = player.position;
                        mainScoreBoard.SCORE += a.ASTEROIDPOINTVALUES[(int)a.mySize];
                        if (a.mySize != Asteroid.SIZEOFASTEROID.SMALL)
                        {
                            for (int x = 0; x < 2; x++)
                            {
                                Asteroid newOne = new Asteroid(this, a.mySize - 1, player.position );
                                newOne.moveAngle = a.moveAngle + randomizer.Next(-25, 25);
                                newOne.newPseudoRandomVelocity(a);
                                asteroids.Add(newOne);
                            }
                        }
                    }

                    if (!EnterHighScoreScreen.display)
                    {
                        a.Draw();
                        a.Move();
                    }
                }
                else if (a.destructionAnimation.radius < a.MAXANIMRADIUS )
                {
                    if (!EnterHighScoreScreen.display)
                    {
                        a.destructionAnimation.radius += 2.25f; 
                        a.Draw(); 
                    }
                }
                else
                    asteroids.RemoveAt(aCur);

            }
            if (player.isActive) 
            {
                player.Draw(); 
                projectiles.Draw();
            }

            if (!EnterHighScoreScreen.display)
            {
                if (ufo.IsActive)
                    ufoprojectiles.Draw();

                ufo.Draw();
            }

            mainScoreBoard.Draw();
            EnterHighScoreScreen.Draw();

        }

        private void initiatePlayerDestroyedSequence()
        {
            player.bPlayerIsDestroyed = true;
            mainScoreBoard.SHIPSLEFT -= 1;
            if (mainScoreBoard.SHIPSLEFT == 0)
            {
                gameOver = true;
                gameOverResetAtTicks = DateTime.Now.Ticks + (4 * TICKSPERSECOND);
            }

            playerDestroyedSFTicks[0] = DateTime.Now.Ticks;
        }

        
        public void onUFOExit()
        {
            mySounds.library[(ufo.ufoType == UFO.UFOSIZE.LARGE)?Sounds.SOUNDS.SPACESHIPLARGE: Sounds.SOUNDS.SPACESHIPSMALL].Stop();
        }

        private void frmAsteroids_Load(object sender, EventArgs e)
        {
            DoubleBuffered = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASTEROIDS
{
    class Projectiles
    {
        public List<Projectile> list = new List<Projectile>();
        private frmAsteroids canvas;
        private long lastTick = 0;

        public Projectiles(frmAsteroids frm)
        {
            canvas = frm;
            lastTick = System.DateTime.Now.Ticks;
        }

        public void Draw()
        {
            foreach (Projectile p in list)
            {
                float sa = (float)Math.Sin(p.firingAngle - ( Math.PI /2 ));
                float ca = (float)Math.Cos(p.firingAngle - (Math.PI / 2));
                float psa = (float)Math.Sin(p.angleOfShipMomentum - (Math.PI / 2));
                float pca = (float)Math.Cos(p.angleOfShipMomentum - (Math.PI / 2));
                double sox = psa * p.shipVelocity;
                double soy = pca * p.shipVelocity;

                Brush currentBrush = (Brush)Brushes.White ;

                // Обчислення нової позиції
                 Point position = new Point( p.position.X + (int)(ca * Projectile.SPEED),
                                             p.position.Y + (int)(sa * Projectile.SPEED  ));
                if (position.X < 0)
                    position = new Point(canvas.Width + position.X, position.Y);
                else if (position.X > canvas.Width)
                    position = new Point(position.X - canvas.Width, position.Y);
                if (position.Y < 0)
                    position = new Point(position.X, canvas.Height + position.Y);
                else if (position.Y > canvas.Height)
                    position = new Point(position.X, position.Y - canvas.Height);
                    
                // Малює лазер
                canvas.g.FillRectangle(currentBrush, position.X, position.Y, 2, 2);

                // Оновлення інформації про снаряди
                p.position = position;
                p.decay += System.DateTime.Now.Ticks - lastTick;
            }

            lastTick = System.DateTime.Now.Ticks;

            // Видалення зруйнованих снарядів
            if (list.Count > 0)
            {
                for (int current = list.Count; current > 0; current--)
                {
                    if ( ( list[current-1].decay / 10000000) > Projectile.DECAYTIME)
                        list.RemoveAt(current-1);
                }
            }
        }

        private long ticksSinceLastFire = 0;
        public void Fire(Ship player, Point value, float currentAngle, double av )
        {
            long currentTicks = System.DateTime.Now.Ticks;
            
            if (list.Count < 4  && ( ( currentTicks - ticksSinceLastFire ) > 1000000 ) )
            {
                ticksSinceLastFire = currentTicks;
                list.Add(new Projectile() { decay = 0.0f, firingAngle = currentAngle, position = value, angulerVelocity = av, shipVelocity = player.velocity, angleOfShipMomentum = player.myAngle });
            }
        }

        public void RandomFire(Point origin)
        {
            long currentTicks = System.DateTime.Now.Ticks;
            float currentAngle = (float)canvas.randomizer.Next(1, 360);
            float av = (float)canvas.randomizer.Next(1, 360);

             
            if (list.Count < 4 && ((currentTicks - ticksSinceLastFire) > 1000000))
            {
                ticksSinceLastFire = currentTicks;
                list.Add(new Projectile() { decay = 0.0f, firingAngle = currentAngle, position = origin, angulerVelocity = av });
            }
        }

        public void NotSoRandomFire(Point origin, float angleOfPlayerFromUFO )
        {
            long currentTicks = System.DateTime.Now.Ticks;
            float currentAngle = angleOfPlayerFromUFO, av = currentAngle;

            
            if (list.Count < 4 && ((currentTicks - ticksSinceLastFire) > 1000000))
            {
                ticksSinceLastFire = currentTicks;
                list.Add(new Projectile() { decay = 0.0f, firingAngle = currentAngle, position = origin, angulerVelocity = av });
            }
        }
    }

    public class Projectile
        {
            public const float DECAYTIME = 1.2f; // за секунду
            public const float SPEED = 19.0f; // Пікселів за секунду

            public float decay = 0.0f;
            public float firingAngle = 0.0f;
            public double angulerVelocity = 0.0f; 
            public Point position = new Point(0, 0);

            
            public double shipVelocity = 0.0f;
            public float angleOfShipMomentum = 0.0f;

        }
}
